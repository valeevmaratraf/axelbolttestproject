using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace TestProject
{
    [RequireComponent(typeof(Button))]
    public class RestartButton : MonoBehaviour
    {
        private Button button;
        // private EndConditionChecker _endGameConditionChecker;
        private GameStarter _gameStarter;

        [Inject]
        private void Construct(GameStarter gameStarter, EndConditionChecker endGameConditionChecker)
        {
            _gameStarter = gameStarter;
            button = GetComponent<Button>();
            button.onClick.AddListener(StartGameButtonClickHandler);
            endGameConditionChecker.OnEndGame += () => gameObject.SetActive(true);
        }

        private void StartGameButtonClickHandler()
        {
            _gameStarter.StartGame();
            gameObject.SetActive(false);
        }
    }
}
