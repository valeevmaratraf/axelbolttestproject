using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace TestProject
{
    public class SettingsSliders : MonoBehaviour
    {
        [SerializeField] private Slider _accelerationSlider;
        [SerializeField] private Slider _speedSlider;
        [SerializeField] private Slider _radiusSlider;

        private DynamicSettings _settings;

        [Inject]
        private void Construct(DynamicSettings settings)
        {
            _settings = settings;
            SubscribeSettingsPropertiesOnSliders();
        }
        private void Start()
        {
            SyncAllProperties();
        }

        private void SubscribeSettingsPropertiesOnSliders()
        {
            _accelerationSlider.onValueChanged.AddListener((value) => _settings.BugAcceleration = value);
            _speedSlider.onValueChanged.AddListener((value) => _settings.MaxBugSpeed = value);
            _radiusSlider.onValueChanged.AddListener((value) => _settings.MoveAwayRadius = value);
        }

        private void SyncAllProperties()
        {
            _settings.BugAcceleration = _accelerationSlider.value;
            _settings.MaxBugSpeed = _speedSlider.value;
            _settings.MoveAwayRadius = _radiusSlider.value;
        }
    }
}
