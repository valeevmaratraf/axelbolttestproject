using System.Collections.Generic;

namespace TestProject
{
    public class GameStarter
    {
        private List<IRestartable> _restartables;

        public GameStarter(EndConditionChecker endConditionChecker, List<IRestartable> restartables)
        {
            _restartables = restartables;
            endConditionChecker.OnEndGame += EndGamehandler;
        }

        private void EndGamehandler()
        {
            foreach (var r in _restartables)
            {
                r.IsEnabled = false;
            }
        }

        public void StartGame()
        {
            foreach (var r in _restartables)
            {
                r.Restart();
                r.IsEnabled = true;
            }
        }
    }
}
