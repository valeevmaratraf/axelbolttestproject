using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestProject
{
    public interface IRestartable
    {
        void Restart();
        bool IsEnabled { get; set; }
    }
}
