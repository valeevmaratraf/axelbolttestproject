using UnityEngine;

namespace TestProject
{
    public static class CameraWorldCornersCalculator
    {
        public static Vector3 GetCorner(Corner corner, Camera camera)
        {
            Vector3 cornerWorldPosition = Vector3.zero;
            switch (corner)
            {
                case Corner.BottomLeft: cornerWorldPosition = new Vector3(0, 0); break;
                case Corner.BottomRight: cornerWorldPosition = new Vector3(Screen.width, 0); break;
                case Corner.TopLeft: cornerWorldPosition = new Vector3(0, Screen.height); break;
                case Corner.TopRight: cornerWorldPosition = new Vector3(Screen.width, Screen.height); break;
            }
            cornerWorldPosition.z = -camera.transform.position.z;
            return camera.ScreenToWorldPoint(cornerWorldPosition);
        }
    }
}
