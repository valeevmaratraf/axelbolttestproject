using System;
using UnityEngine;

namespace TestProject
{
    public class DynamicSettings : IObservable
    {
        private float _bugSpeed;
        private float _bugAcceleration;
        private float _moveAwayRadius;

        public float MaxBugSpeed
        {
            get => _bugSpeed;
            set
            {
                if (value == _bugSpeed)
                    return;

                _bugSpeed = value;
                PropertyChanged?.Invoke();
            }
        }
        public float BugAcceleration
        {
            get => _bugAcceleration;
            set
            {
                if (value == _bugAcceleration)
                    return;

                _bugAcceleration = value;
                PropertyChanged?.Invoke();
            }
        }
        public float MoveAwayRadius
        {
            get => _moveAwayRadius;
            set
            {
                if (value == _moveAwayRadius)
                    return;

                _moveAwayRadius = value;
                PropertyChanged?.Invoke();
            }
        }

        public event PropertyChanged PropertyChanged;
    }
}
