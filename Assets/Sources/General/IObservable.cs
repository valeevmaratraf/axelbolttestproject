using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

using UnityEngine;

namespace TestProject
{
    public delegate void PropertyChanged([CallerMemberName] string property = "");

    public interface IObservable
    {
        public event PropertyChanged PropertyChanged;
    }
}
