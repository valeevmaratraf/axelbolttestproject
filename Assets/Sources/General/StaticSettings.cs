using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestProject
{
    [CreateAssetMenu(fileName = "StaticAppSettings", menuName = "Custom assets/Static App Settings")]
    public class StaticSettings : ScriptableObject
    {
        [field: SerializeField] public float BugDeccelerationAfterRunningAway { get; private set; }
        [field: SerializeField] public float BugAdditionalMoveAwayDistance { get; private set; }
    }
}
