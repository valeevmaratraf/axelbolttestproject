using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace TestProject
{
    public class SceneInstaller : MonoInstaller
    {
        [SerializeField] private Camera _gameCamera;
        [SerializeField] private Transform _bugMoveTarget;
        [SerializeField] private StaticSettings _staticSettings;
        [SerializeField] private MoveAwayPosition _moveAwayPosition;
        [SerializeField] private Transform _bugsRoot;
        private DynamicSettings _dynamicSettings;
        private EndConditionChecker _endConditionChecker;
        private List<Bug> _bugs;
        private GameStarter _gameStarter;

        public override void InstallBindings()
        {
            _dynamicSettings = new DynamicSettings();
            _bugs = new List<Bug>(_bugsRoot.GetComponentsInChildren<Bug>());
            _endConditionChecker = new EndConditionChecker(_bugs);
            _gameStarter = new GameStarter(_endConditionChecker, GetRestartableObjects());

            Container.Bind<EndConditionChecker>().FromInstance(_endConditionChecker).AsSingle();
            Container.Bind<GameStarter>().FromInstance(_gameStarter).AsSingle();
            Container.Bind<DynamicSettings>().FromInstance(_dynamicSettings).AsSingle();
            Container.Bind<MoveAwayPosition>().FromInstance(_moveAwayPosition).AsSingle();
            Container.Bind<Camera>().FromInstance(_gameCamera).AsSingle();
            Container.Bind<StaticSettings>().FromInstance(_staticSettings).AsSingle();
            Container.Bind<List<Bug>>().FromInstance(_bugs).AsSingle();
            Container.Bind<BugMovementController.ConstructArgs>().FromInstance(GetMovementArgs()).AsSingle();
            Container.Bind<GameCycleController.ConstructArgs>().FromInstance(GetGameCycleControllerArg()).AsSingle();
        }

        private BugMovementController.ConstructArgs GetMovementArgs()
        {
            return new BugMovementController.ConstructArgs()
            {
                Settings = _dynamicSettings,
                MoveTarget = _bugMoveTarget,
                AppSettings = _staticSettings,
            };
        }

        private GameCycleController.ConstructArgs GetGameCycleControllerArg()
        {
            return new GameCycleController.ConstructArgs()
            {
                Bugs = _bugs,
                EndConditionChecker = _endConditionChecker,
                MoveAwayPosition = _moveAwayPosition,
            };
        }

        private List<IRestartable> GetRestartableObjects()
        {
            var restartables = new List<IRestartable>(16);
            foreach (var root in SceneManager.GetActiveScene().GetRootGameObjects())
            {
                restartables.AddRange(root.GetComponentsInChildren<IRestartable>());
            }
            return restartables;
        }
    }
}
