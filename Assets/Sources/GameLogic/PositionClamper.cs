using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace TestProject
{
    public class PositionClamper : MonoBehaviour
    {
        [SerializeField] private Transform _target;

        [SerializeField] private float _offsetX;
        [SerializeField] private float _offsetY;

        private Vector3 _leftTopCorner;
        private Vector3 _rightBottomCorner;

        [Inject]
        private void Construct(Camera _gameCamera)
        {
            if (_target == null)
            {
                _target = transform;
            }

            _leftTopCorner = CameraWorldCornersCalculator.GetCorner(Corner.TopLeft, _gameCamera);
            _rightBottomCorner = CameraWorldCornersCalculator.GetCorner(Corner.BottomRight, _gameCamera);
        }

        public void Clamp()
        {
            var position = _target.position;
            position.x = Mathf.Clamp(position.x, _leftTopCorner.x + _offsetX, _rightBottomCorner.x - _offsetX);
            position.y = Mathf.Clamp(position.y, _rightBottomCorner.y + _offsetY, _leftTopCorner.y - _offsetY);
            _target.position = position;
        }
    }
}
