using System;
using System.Collections.Generic;
using Zenject;

namespace TestProject
{
    public class EndConditionChecker
    {
        private List<Bug> _bugs;

        public Action OnEndGame;

        public EndConditionChecker(List<Bug> bugs)
        {
            _bugs = bugs;
        }

        public void CheckEndCondition()
        {
            foreach (var b in _bugs)
            {
                if (b.IsOnFinish && b)
                {
                    OnEndGame?.Invoke();
                }
            }
        }
    }
}
