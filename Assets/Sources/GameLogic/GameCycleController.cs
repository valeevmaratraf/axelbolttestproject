using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace TestProject
{
    public class GameCycleController : MonoBehaviour
    {
        private List<Bug> _bugs;
        private MoveAwayPosition _moveAwayPosition;
        private EndConditionChecker _endConditionChecker;

        [Inject]
        private void Construct(ConstructArgs args)
        {
            _bugs = args.Bugs;
            _moveAwayPosition = args.MoveAwayPosition;
            _endConditionChecker = args.EndConditionChecker;
        }

        private void Update()
        {
            _moveAwayPosition.UpdateMoveAwayPointPosition();
            foreach (var b in _bugs)
            {
                b.UpdatePosition(_moveAwayPosition.Position);
            }
            _endConditionChecker.CheckEndCondition();
        }

        public class ConstructArgs
        {
            public List<Bug> Bugs;
            public MoveAwayPosition MoveAwayPosition;
            public EndConditionChecker EndConditionChecker;
        }
    }
}
