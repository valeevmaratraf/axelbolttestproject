using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestProject
{
    [RequireComponent(typeof(BugMovementController), typeof(PositionClamper), typeof(CornerPositionSetter))]
    public class Bug : MonoBehaviour, IRestartable
    {
        private BugMovementController _bugMovementController;
        private PositionClamper _positionClamper;
        private CornerPositionSetter _cornerPositionSetter;

        public bool IsOnFinish => _bugMovementController.IsMoveTargetReached;

        public bool IsEnabled
        {
            get => gameObject.activeInHierarchy;
            set => gameObject.SetActive(value);
        }

        private void Awake()
        {
            _bugMovementController = GetComponent<BugMovementController>();
            _positionClamper = GetComponent<PositionClamper>();
            _cornerPositionSetter = GetComponent<CornerPositionSetter>();
        }

        public void UpdatePosition(Vector3 moveAwayPosition)
        {
            if (!gameObject.activeInHierarchy)
            {
                return;
            }

            _bugMovementController.UpdatePosition(moveAwayPosition);
            _positionClamper.Clamp();
        }

        public void Restart()
        {
            _cornerPositionSetter.SetPosition();
            _bugMovementController.ResetState();
        }
    }
}
