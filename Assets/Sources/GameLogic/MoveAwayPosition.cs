using UnityEngine;
using Zenject;

namespace TestProject
{
    public class MoveAwayPosition : MonoBehaviour
    {
        private Transform _transform;
        private Camera _gameCamera;
        private DynamicSettings _dynamicSettings;

        private const float SHAPE_SCALE_COEFF = 1.52F;

        public Vector3 Position => _transform.position;

        [Inject]
        private void Construct(Camera gameCamera, DynamicSettings settings)
        {
            _transform = transform;
            _gameCamera = gameCamera;
            _dynamicSettings = settings;
            _dynamicSettings.PropertyChanged += SettingsPropertyChangeHandler;
        }

        private void SettingsPropertyChangeHandler(string propertyName)
        {
            if (propertyName == nameof(_dynamicSettings.MoveAwayRadius))
            {
                UpdateRadius();
            }
        }

        private void UpdateRadius()
        {
            transform.localScale = Vector3.one * _dynamicSettings.MoveAwayRadius * SHAPE_SCALE_COEFF;
        }

        public void UpdateMoveAwayPointPosition()
        {
            var screenMousePosition = Input.mousePosition;
            screenMousePosition.z = -_gameCamera.transform.position.z;
            var worldMousePosition = _gameCamera.ScreenToWorldPoint(screenMousePosition);
            _transform.position = worldMousePosition;
        }
    }
}
