using UnityEngine;
using Zenject;

namespace TestProject
{
    public class BugMovementController : MonoBehaviour
    {
        //References:
        private Transform _transform;
        private DynamicSettings _dynamicSettings;
        private Transform _moveTarget;
        private StaticSettings _staticSettings;

        //Own fields:
        private float _currentSpeed;
        private float _additionalAwayMovedDistance;

        //Public properties
        public bool IsMoveTargetReached => _transform.position == _moveTarget.position;

        [Inject]
        private void Construct(ConstructArgs args)
        {
            _dynamicSettings = args.Settings;
            _moveTarget = args.MoveTarget;
            _transform = transform;
            _staticSettings = args.AppSettings;

            _additionalAwayMovedDistance = _staticSettings.BugAdditionalMoveAwayDistance;
        }

        public void UpdatePosition(Vector3 moveAwayPosition)
        {
            if (IsMoveTargetReached)
            {
                return;
            }

            var sqrDistanceToMoveAwayPosition = Vector3.SqrMagnitude(moveAwayPosition - _transform.position);
            bool isRuningAway = sqrDistanceToMoveAwayPosition < Mathf.Pow(_dynamicSettings.MoveAwayRadius, 2);
            bool additionalMoveCompleted = _additionalAwayMovedDistance >= _staticSettings.BugAdditionalMoveAwayDistance;

            Vector3 moveDirection = isRuningAway || !additionalMoveCompleted ?
                _transform.position - moveAwayPosition :
                _moveTarget.position - _transform.position;

            float acceleration = _dynamicSettings.BugAcceleration;
            if (!isRuningAway && _currentSpeed > _dynamicSettings.MaxBugSpeed)
            {
                acceleration = -_staticSettings.BugDeccelerationAfterRunningAway;
            }

            _currentSpeed += acceleration * Time.deltaTime;

            if (!isRuningAway && acceleration > 0)
            {
                _currentSpeed = Mathf.Clamp(_currentSpeed, 0, _dynamicSettings.MaxBugSpeed);
            }

            _transform.up = moveDirection;
            var frameMoveDelta = transform.up * Time.deltaTime * _currentSpeed;

            if (!isRuningAway)
            {
                _additionalAwayMovedDistance += frameMoveDelta.magnitude;
            }
            else
            {
                _additionalAwayMovedDistance = 0;
            }

            bool isFinishReached = Vector3.SqrMagnitude(frameMoveDelta) > Vector3.SqrMagnitude(moveDirection);
            if (isFinishReached && !isRuningAway)
            {
                _transform.position = _moveTarget.position;
                return;
            }

            _transform.position += frameMoveDelta;
        }

        public void ResetState()
        {
            _additionalAwayMovedDistance = _staticSettings.BugAdditionalMoveAwayDistance;
            _currentSpeed = 0;
        }

        public class ConstructArgs
        {
            public DynamicSettings Settings;
            public Transform MoveTarget;
            public StaticSettings AppSettings;
        }
    }
}
