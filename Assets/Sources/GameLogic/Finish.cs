using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TestProject
{
    [RequireComponent(typeof(CornerPositionSetter))]
    public class Finish : MonoBehaviour, IRestartable
    {
        private CornerPositionSetter _positionSetter;

        public bool IsEnabled
        {
            get => gameObject.activeInHierarchy;
            set => gameObject.SetActive(value);
        }


        private void Awake()
        {
            _positionSetter = GetComponent<CornerPositionSetter>();
        }

        public void Restart()
        {
            _positionSetter.SetPosition();
        }

        public void SetEnable(bool value) => gameObject.SetActive(value);
    }
}
