using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace TestProject
{
    public enum Corner { TopLeft, TopRight, BottomLeft, BottomRight }

    public class CornerPositionSetter : MonoBehaviour
    {
        [SerializeField] private Transform _target;
        [SerializeField] private Corner _corner;
        [SerializeField] private Vector3 _offset;

        private Camera _gameCamera;

        [Inject]
        private void Construct(Camera gameCamera)
        {
            if (_target == null)
            {
                _target = transform;
            }
            _gameCamera = gameCamera;
        }

        public void SetPosition()
        {
            var cornerPosition = CameraWorldCornersCalculator.GetCorner(_corner, _gameCamera);
            _target.position = cornerPosition + _offset;
        }
    }
}
